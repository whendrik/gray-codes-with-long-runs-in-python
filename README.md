# Gray Codes with long Runs in Python

This is a python implementation of Method 1 described in [Gray Codes with Optimized Run Lengths](https://www.researchgate.net/publication/246342379_Gray_Codes_with_Optimized_Run_Lengths) with special case for `n=10` bits from [Binary gray codes with long bit runs](http://emis.impa.br/EMIS/journals/EJC/Volume_10/PDF/v10i1r27.pdf)

Both papers suggests methods to improve the generated gaps, for instance for `n = 21`